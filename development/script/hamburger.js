(()=> {
    'use strict';

    let navigation = document.querySelector('.navigation');
    let hamburger = document.querySelector('.navigation__hamburger');

    if(!navigation || !hamburger) {
        return;
    }

    const checkDevice = () => {
        return (window.matchMedia('screen and (max-width: 768px)').matches || navigator.userAgent.match(/(iPhone|iPod|iPad|Android)/));
    }

    const handleClick = () => {
        if(!navigation.classList.contains('navigation--open')) {
            navigation.classList.add('navigation--shut');

            setTimeout(() => {
                navigation.classList.remove('navigation--shut');
                navigation.classList.add('navigation--open');
                document.body.classList.add('nav-open');
            }, 500)

        } else {
            navigation.classList.add('navigation--shut');
            navigation.classList.remove('navigation--open');

            setTimeout(() => {
                navigation.classList.remove('navigation--shut');
                document.body.classList.remove('nav-open');

            }, 500)

        }
    }
   

    window.addEventListener('resize', function() {
        navigation.classList.remove('navigation--open');
        document.body.classList.remove('nav-open');
    });

    window.addEventListener('scroll', function() {
        navigation.classList.remove('navigation--open');
        document.body.classList.remove('nav-open');
    });

    hamburger.addEventListener('click', handleClick);


})();