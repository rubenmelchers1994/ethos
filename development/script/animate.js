(function() {
	"use strict";

	let elements = {
			Static: document.querySelectorAll('.fadeIn'),
			Up: document.querySelectorAll('.fadeInUp'),
			Down: document.querySelectorAll('.fadeInDown'),
			Left: document.querySelectorAll('.fadeInLeft'),
			Right: document.querySelectorAll('.fadeInRight'),
			UpBig: document.querySelectorAll('.fadeInUpBig')
		};
	let parallaxElements = {
			Up: document.querySelectorAll('.parallaxUp'),
			Down: document.querySelectorAll('.parallaxDown'),
		};
	let folios = document.querySelectorAll('.folio__link');
	let	w = window;

	if(!elements || elements.length < 0) {
		return;
	}

	w.addEventListener('scroll', setAnimationFrame, false);

	setAnimations();
	function setAnimationFrame() {
		w.requestAnimationFrame(setAnimations);
	}


	function setAnimations() {
		for (let direction in elements) {
			if(elements.hasOwnProperty(direction)) {
				for(let i = 0; i < elements[direction].length; i++) {
					fadeOnShow(elements[direction][i], direction);
				}
			}
		}

		for ( let key in parallaxElements) {
            if (parallaxElements.hasOwnProperty(key)) {
                for (let i = 0; i < parallaxElements[key].length; i++) {
                    parallax(parallaxElements[key][i], key);
                }
            }
		}

		if(folios.length > 0) {
			folios.forEach((folio) => {
				activateOnEnter(folio);
			})
			
		}
	}

    /** Function for elements with parallax classes */
	function parallax (el, direction) {
		var animationMin = el.getBoundingClientRect().top - w.innerHeight - 200,
			animationMax = animationMin + el.offsetHeight + w.innerHeight,
			animateSpeed = 9;

		if(direction == 'Up') {
			/* Align element a bit to the bottom */
			el.style.top = `${animateSpeed * 5}px`;

			/* animate element up */
			if(animationMin < 0 && animationMax > 0) {
				el.style.transform = `translateY(-${animationMax / (animateSpeed * 1)}px)`;
			}

		} else {
			/* Align element a bit to the top */
			el.style.top = `-${animateSpeed * 5}px`;

			/* animate element up */
			if(animationMin < 0 && animationMax > 0) {
				el.style.transform = `translateY(${animationMax / (animateSpeed * 1)}px)`;
			}
		}
	}

	function fadeOnShow(el, direction) {
		let animateMin = checkPosition(el);

		if(direction == 'Static') {
			direction = '';
		}

		if(animateMin) {
			el.classList.add('fadeIn' + direction + '--animate');
		} else  {
			el.classList.remove('fadeIn' + direction + '--animate');
		}

	}

	function activateOnEnter(el) {
		let animateMin = checkFolioPos(el);

		if(animateMin) {
			el.classList.add('folio__link--active');
		} else  {
			el.classList.remove('folio__link--active');
		}

	}

	function checkPosition(element) {

		let rect = element.getBoundingClientRect(),
			viewHeight = Math.max(document.documentElement.clientHeight, w.innerHeight) / 1.2;

		return !(rect.bottom < 0 || rect.top - viewHeight >= 0);

	}

	function checkFolioPos(element) {

		let rect = element.getBoundingClientRect(),
			viewHeight = Math.max(document.documentElement.clientHeight, w.innerHeight) / 2;

		return !(rect.bottom < 500 || rect.top - viewHeight >= 0);

	}

})();
