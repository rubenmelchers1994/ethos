(()=> {
    'use strict';

    const title = document.querySelector('.header__title');

    if(!title) {
        return;
    }

    const highlighted = title.dataset.highlighted;

    if(!highlighted) {
        return;
    }

    const titleText = title.innerText;

    if(titleText.indexOf(highlighted) >= 0) {
        title.innerHTML = title.innerHTML.replace(highlighted, `<span class="header__highlighted">${highlighted}</span>`);
    }
    
})();