(() => {
    'use strict';

    const carousels = document.querySelectorAll('modal__carousel-wrapper');

    if(carousels.length <= 0) {
        return;
    }

    carousels.forEach((carousel) => {
        initCarousel(carousel);
    });

    const initCarousel = (element)  => {
        let slider = element.querySelector('.modal__carousel-slider');
        let images = slider.querySelectorAll('img');

        let total = images.length;
        let current = 0;

        let navLeft = element.querySelector('.modal__carousel-nav--left');
        let navRight = element.querySelector('.modal__carousel-nav--right');

        images[0].classList.add('modal__carousel-img--active');

        navLeft.addEventListener('click', () => {
            current -= 1;
            console.log(current);
        });

        navRight.addEventListener('click', () => {
            current += 1;
            console.log(current);

        });

        // console.log(current);


        if(current > total) {
            current = 0;
        } else if (current < 0) {
            current = total;
        }

    }

})();