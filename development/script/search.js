(function() {
    'use strict';


    let navigation  = document.querySelector('.navigation');

    if(!navigation) {
        return;
    }

    let search      = navigation.querySelector('.navigation__search'),
        searchbox   = navigation.querySelector('.navigation__searchbox');

    if (!search || !searchbox) {
        return;
    }

    search.addEventListener('click', function() {
        search.classList.toggle('navigation__search--active');
        searchbox.classList.toggle('navigation__searchbox--active');
    });

})();
