(function() {
    'use strict';

    let navbar = document.querySelector('.navigation');

    if(!navbar) {
        return;
    }

    let w = window;

    w.addEventListener('scroll', () => {
        handleNav();
        setAnimationFrame();
    });

    
    const handleNav = () => {
        if(w.pageYOffset >= 200) {
            navbar.classList.add('navigation--small')
        } else {
            navbar.classList.remove('navigation--small');
        }
    }

    const checkPosition = (element) => {

		let rect = element.getBoundingClientRect(),
			viewHeight = Math.max(document.documentElement.clientHeight, w.innerHeight) / 1.2;

		return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    const header = document.querySelector('.header');
    if(!header) {
        return;
    }
    const title = header.querySelector('.header__title');

    if(!title) {
        return;
    }

    w.addEventListener('load', () => {
        animateTitle();
        // setTimeout(() => {
        // }, 1000)
    });

    setAnimations();
    function setAnimationFrame() {
        w.requestAnimationFrame(setAnimations);
    }

    function setAnimations() {
        let animateMin = checkPosition(title);
        if(animateMin) {
            title.style.top = (w.pageYOffset / 4) + 'px';
        }
    }

    const animateTitle = () => {
        const highlightElem = title.querySelector('.header__highlighted');

        if(highlightElem) {
            highlightElem.classList.add('header__highlighted--show')
        }
    }


})();
