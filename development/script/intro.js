(function() {
    'use strict';


    let logo    = document.querySelector('.intro__image'),
        w       = window;

    if(logo) {
        w.addEventListener('scroll', setAnimationFrame, false);

        setAnimations();
        function setAnimationFrame() {
            w.requestAnimationFrame(setAnimations);
        }
    
        function setAnimations() {
            let animateMin = checkPosition(logo);
            if(animateMin) {
                logo.style.top = 320 + (window.pageYOffset / 2) + 'px';
            }
        }
    }

    function checkPosition(element) {

		let rect = element.getBoundingClientRect(),
			viewHeight = Math.max(document.documentElement.clientHeight, w.innerHeight) / 1.2;

		return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }
    
    let subtitleItems = Array.from(document.querySelectorAll('.intro__subtitle-item'));
    let interval;
    let slide = 0;

    if(subtitleItems.length > 0) {
        
        interval = setInterval(function(){
            animateSubtitles();
		}, 0);     
    }
                    
    const animateSubtitles = () => {
        clearInterval(interval);
        let lastIndex = subtitleItems.length - 1;

        subtitleItems.forEach(function(el){
            el.classList.remove('intro__subtitle-item--previous');
            el.classList.remove('intro__subtitle-item--current');
            el.classList.remove('intro__subtitle-item--next');
        })
        
        let previous = subtitleItems[slide-1];
        let current = subtitleItems[slide];
        let next = subtitleItems[slide+1];

        if(slide <= 0) {
            previous = subtitleItems[lastIndex];
        }
        if(slide >= lastIndex) {
            next = subtitleItems[0];
        }

        previous.classList.add('intro__subtitle-item--previous');
        current.classList.add('intro__subtitle-item--current');
        next.classList.add('intro__subtitle-item--next');

        slide++;
        if(slide > lastIndex) {
            slide = 0;
        }

        interval = setInterval(function(){
			animateSubtitles();
		}, 10000);
    }

})();
