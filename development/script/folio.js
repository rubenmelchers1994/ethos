(() => {
    'use strict';

    const links = document.querySelectorAll('.folio__link');
    const modals = document.querySelectorAll('.modal');
    const body = document.body;

    if (links.length <= 0 || modals.length <= 0) {
        return;
    }

    let activeFolio;

    links.forEach((link) => {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            let linkedFolio = link.dataset.folio;
            if (linkedFolio) {
                let folioToActivate = document.querySelector(`[data-folio-modal='${linkedFolio}']`);

                if (folioToActivate) {
                    folioToActivate.classList.add('modal--active');
                    body.classList.add('scroll-lock');
                    activeFolio = folioToActivate;
                }
            }
        });
    });

    const closeFolios = () => {
        body.classList.remove('scroll-lock');
        let foliosToDeactivate = document.querySelectorAll('.modal--active');
        foliosToDeactivate.forEach((folio) => {
            folio.classList.remove('modal--active');
        })
    }

    body.addEventListener('click', (e) => {
        // let folioWrapper = document.querySelector('#folio');
        // if (!folioWrapper.contains(e.target) && body.classList.contains('scroll-lock')) {
        //     closeFolios();
        // }
        if (e.target.classList.contains('modal--active') || e.target.classList.contains('js-modal-close')) {
            closeFolios();
        }
    })


})();