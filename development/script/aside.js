(function() {
    'use strict';

    let aside = document.querySelector('.servicebody__aside-wrapper');
    let w = window;
    let form = document.querySelectorAll('.form')[0];

    if(!aside || window.matchMedia("(max-width: 1024px)").matches) {
        return;
    }

    setTimeout(function() {
        // console.log('done');
        aside.classList.add('servicebody__aside-wrapper--animated');
    }, 2000);

    if(form) {
        let button = aside.querySelector('.servicebody__aside-button');

        button.href = '#' + form.id;
    }

	const setAnimationFrame = () => {
		w.requestAnimationFrame(parallax); //use requestAnimationFrame for a better/smoother performance and is also more battery-friendly.
    }

	const parallax = () => {
        let _scrollTop = (document.documentElement.scrollTop || document.body.scrollTop);

        aside.classList.add('servicebody__aside-wrapper--animated');

        if(!form) {
            aside.style.transform = 'translateY(' + _scrollTop / 2 + 'px)';
        } else if(form && _scrollTop < form.offsetTop - 300) {
            aside.style.transform = 'translateY(' + _scrollTop / 2 + 'px)';
        }

        if(form) {
            console.log(_scrollTop, form.offsetTop - 200);
        }
	}
    
    parallax();

    w.addEventListener('resize', () => {
        w = window;
        setAnimationFrame();
    })

    w.addEventListener('scroll', () => {
        w = window;
        // console.log(w);
        setAnimationFrame();
    })


})();