<?php

    global $siteLang;

    $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

    if (strpos($url,'nl') !== false) {
        // echo 'site is dutch.';
        $siteLang = 'nl';
    } else if (strpos($url,'pl') !== false) {
        // echo 'site is polish.';
        $siteLang = 'pl';
    } else if (strpos($url,'en') !== false) {
        // echo 'site is english';
        $siteLang = 'en';
    } else {
        // echo 'site fallback language (en)';
        $siteLang = 'en';
    }

?>
