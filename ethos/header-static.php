<?php

    $page_slug = get_post_field( 'post_name', get_post() );
    $post_type = get_post_field( 'post_type', get_post() );

?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

    <!-- FAVICON -->

	<?php wp_head(); ?>

    <meta property="article:author" content="Ethos" />
    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo( 'name' ); ?> Feed" href="<?php echo esc_url( get_feed_link() ); ?>">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

</head>
<body>
    <nav class="navigation">
        <ul class="grid-12 container navigation__list">
                <li class="col-4 navigation__list-item navigation__list-item--logo">
                    <a href="<?php echo home_url( '/' ); ?>" class="navigation__logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="ethos logo" class="navigation__logo-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-visual.png" alt="" class="navigation__logo-image navigation__logo-image--visual">
                    </a>
                </li>
                <li class="col-8">
                    <ul class="grid-12 navigation__sublist">
                        <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#ethos">
                            <span class="navigation__link-inner">
                                Ethos
                            </span>
                        </a>
                        </li>
                        <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#folio">
                            <span class="navigation__link-inner">
                                Folio
                            </span>
                        </a>
                        </li>
                        <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#contact">
                            <span class="navigation__link-inner">
                                Contact
                            </span>
                        </a>
                        </li>

                    </ul>
                </li>
        </ul>
        <div class="navigation__hamburger">
            <div class="bar"></div>
        </div>
    </nav>

<main role="main" data-type="<?php echo $post_type; ?>" class="<?php echo $page_slug; ?> <?php echo 'slug__'.$page_slug; ?> <?php echo 'type__'.$post_type; ?>">
