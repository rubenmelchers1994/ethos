<?php

// Load CSS & JS files
function wp_adding_css_js(){
	// wp_enqueue_style( 'app-css', get_template_directory_uri().'/assets/css/app.css');
    // wp_enqueue_style( 'pitcher-css', get_template_directory_uri().'/assets/css/pitcher.css');
    // wp_enqueue_style( 'google-css', 'https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600,700');

	// CSS
	// wp_enqueue_style( 'main-css', get_template_directory_uri() . '/theme-files/website/css/style.css' );

	// Javascripts
	// wp_enqueue_script( 'main-js', get_template_directory_uri() . '/theme-files/website/js/main.js', array('jquery'), THEME_VERSION, true );

    // USE THIS ONE WHEN LIVE
    //wp_enqueue_script( 'main-js', get_template_directory_uri() . '/theme-files/website/js/main-min.js', array('jquery'), THEME_VERSION, true );
}

add_action( 'wp_enqueue_scripts', 'wp_adding_css_js' );


// Move JavaScript code to page footer
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);

?>
