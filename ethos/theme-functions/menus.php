<?php
/*=========================================
	MENUS
===========================================
	AREAS
-----------------------------------------*/
register_nav_menus(
	array(
		'main-menu'   => __( 'Main Menu' ),
		//'extra-menu'  => __( 'Extra' ),
		'footer-menu'  => __( 'Footer menu' )
	)
);
/*-----------------------------------------
	ARCHIVES
-----------------------------------------*/
function remove_menu_parent_classes( $class ) {
	return ($class == 'active') ? FALSE : TRUE;
}
function add_menu_parent_class( $classes ) {
	switch ( get_post_type() ) :

	case 'example':
		$classes = array_filter( $classes, 'remove_menu_parent_classes' );
		if ( in_array( 'example', $classes ) ) {
			$classes[] = 'active';
		};
		break;

	endswitch;
	return $classes;
};
add_filter( 'nav_menu_css_class', 'add_menu_parent_class' );
/*-----------------------------------------
	WALKER
-----------------------------------------*/
function is_element_empty( $element ) {
	$element = trim( $element );
	return empty( $element ) ? false : true;
}
class Nav_Walker extends Walker_Nav_Menu {
	function check_current( $classes ) {
		return preg_match( '/(current[-_])|active|dropdown/', $classes );
	}
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n<ul class=\"menu-dropdown\">\n";
	}
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$item_html = '';
		parent::start_el( $item_html, $item, $depth, $args );

		// dropdown link
		if ( $item->is_dropdown && ( $depth === 0 ) ) {
			$item_html = str_replace( '<a', '<a class="toggle-dropdown" id="link-'.$item->ID.'" ', $item_html );
		}
		elseif ( stristr( $item_html, 'li class="divider' ) ) {
			$item_html = preg_replace( '/<a[^>]*>.*?<\/a>/iU', '', $item_html );
		}
		elseif ( stristr( $item_html, 'li class="dropdown-header' ) ) {
			$item_html = preg_replace( '/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html );
		} else {
			$item_html = str_replace( '<a', '<a id="link-'.$item->ID.'" ', $item_html );
		}
		$item_html = apply_filters( 'roots_wp_nav_menu_item', $item_html );
		$output .= $item_html;
	}
	function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
		$element->is_dropdown = ( ( ! empty( $children_elements[$element->ID] ) && ( ( $depth + 1 ) < $max_depth || ( $max_depth === 0 ) ) ) );
		if ( $element->is_dropdown ) {
			$element->classes[] = 'has-dropdown';
		}
		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}
function roots_nav_menu_css_class( $classes, $item ) {
	$slug = sanitize_title( $item->title );
	$classes = preg_replace( '/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes );
	$classes = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );
	$classes[] = 'menu-' . $slug;
	$classes = array_unique( $classes );
	return array_filter( $classes, 'is_element_empty' );
}
add_filter( 'nav_menu_css_class', 'roots_nav_menu_css_class', 10, 2 );
add_filter( 'nav_menu_item_id', '__return_null' );
// DEFAULTS
function roots_nav_menu_args( $args = '' ) {
	$roots_nav_menu_args['container']   = false;
	$roots_nav_menu_args['fallback_cb'] = false;
	$roots_nav_menu_args['items_wrap']  = '<h4 class="hidden">Menu</h4><ul class="menu">%3$s</ul>';
	if ( ! $args['walker'] ) {
		$roots_nav_menu_args['walker'] = new Nav_Walker();
	}
	return array_merge( $args, $roots_nav_menu_args );
}
add_filter( 'wp_nav_menu_args', 'roots_nav_menu_args' );
