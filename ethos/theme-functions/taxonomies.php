<?php
/*=========================================
	TAXONOMIES
===========================================
	UNREGISTER
-----------------------------------------*/
function remove_default_taxonomies() {
	//register_taxonomy( 'category', array(), array( 'public' => false ) );
	//register_taxonomy( 'post_tag', array(), array( 'public' => false ) );
	register_taxonomy( 'link_category', array(), array( 'public' => false ) );
}
add_action( 'init', 'remove_default_taxonomies' );
/*-----------------------------------------
	EXAMPLE_CAT
-----------------------------------------*/
function taxonomy_example_cat() {
	register_taxonomy(
		'example_cat',
		array('example'),
		array(
			'labels' => array(
				'name'              => __( 'Type' ),
				'singular_name'     => __( 'Type' ),
				'search_items'      => __( 'Zoeken' ),
				'popular_items'     => __( 'Meest gebruikt' ),
				'all_items'         => __( 'Allemaal' ),
				'parent_item'       => __( 'Type' ),
				'parent_item_colon' => __( 'Type:' ),
				'edit_item'         => __( 'Bewerken' ),
				'update_item'       => __( 'Bijwerken' ),
				'add_new_item'      => __( 'Toevoegen' ),
				'new_item_name'     => __( 'Nieuwe naam' ),
			),
			'public'              => true,
			'show_in_nav_menus'   => true,
			'show_admin_column'   => true,
			'exclude_from_search' => true,
			'hierarchical'        => true,
			'rewrite'             => array( 'with_front' => true, 'hierarchical' => true, ),
		)
	);
}
add_action( 'init', 'taxonomy_example_cat' );
/*-----------------------------------------
	Standaard inbegrepen
-----------------------------------------*/
function taxonomy_standaard_inbegrepen() {
	register_taxonomy(
		'standaard_inbegrepen',
		array('model'),
		array(
			'labels' => array(
				'name'              => __( 'Standaard' ),
				'singular_name'     => __( 'Standaard' ),
				'search_items'      => __( 'Zoeken' ),
				'popular_items'     => __( 'Meest gebruikt' ),
				'all_items'         => __( 'Allemaal' ),
				'parent_item'       => __( 'Standaard' ),
				'parent_item_colon' => __( 'Standaard:' ),
				'edit_item'         => __( 'Bewerken' ),
				'update_item'       => __( 'Bijwerken' ),
				'add_new_item'      => __( 'Toevoegen' ),
				'new_item_name'     => __( 'Nieuwe naam' ),
			),
			'public'              => true,
			'show_in_nav_menus'   => true,
			'show_admin_column'   => true,
			'exclude_from_search' => true,
			'hierarchical'        => true,
			'rewrite'             => array( 'with_front' => true, 'hierarchical' => true, ),
		)
	);
}
add_action( 'init', 'taxonomy_standaard_inbegrepen' );
