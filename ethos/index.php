<?php
get_header();

if ( is_home() ) :
	get_template_part( 'templates/content', get_post_type() );
elseif ( is_search() ) :
	get_template_part( 'templates/content', 'search' );
else :
	get_template_part( 'templates/content', 'page' );
endif;

?>
 <?php
get_footer();
?>
