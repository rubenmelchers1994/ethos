<?php
	include('site-localization.php');
?>

</main>
<!-- Footer -->
<footer class="footer">

	<ul class="grid-12 container footer__list">

		<li class="col-2 footer__list-item footer__list-item--copyright">
			© KBNA, 2018
		</li>

		<?php

		$args = array(
			'menu' => 'footer ' . $siteLang,
			'container' => '',
			'items_wrap' => '%3$s',
			'menu_class' => 'footer',
			'link_before' => '<span class="footer__text">',
			'link_after' => '</span>'
		);
		wp_nav_menu($args);
		?>
	</ul>

</footer>
<!-- End Footer -->

<?php
	wp_reset_query();  // Restore global post data stomped by the_post().
?>


<?php
//LOAD HEADER SCRIPTS
if( have_rows('scripts', 'option') ):
    while( have_rows('scripts', 'option') ): the_row();
        $locatie = get_sub_field('locatie');
        if ($locatie == 'Footer') {
            echo    get_sub_field('script');
        }
    endwhile;
endif;
?>


<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>
</body>
</html>
