</main>
<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">

			<!-- START OF COLUMN 1 -->
			<?php
				if( have_rows('footer_col_1', 'options') ):
					while( have_rows('footer_col_1', 'options') ): the_row();
					$title = get_sub_field('col1_titel');
			?>

			<ul class="col-sm-6 col-md-3">
				<li><h3><?php echo $title ?></h3></li>
				<?php
				if( have_rows( 'col1_links', 'options' )):
					while ( have_rows( 'col1_links', 'options' ) ) : the_row();

						$link = get_sub_field('link');
						echo '<li><a href="' . $link['url'] . '" target="_blank" >' . $link['title'] . ' </a></li>';

					endwhile;

				else :
				endif;
				?>
			</ul>

		<?php endwhile; endif; ?>
		<!-- END OF COLUMN 1 -->


		<!--  START OF COLUMN 2  -->
		<?php
			if( have_rows('footer_col_2', 'options') ):
				while( have_rows('footer_col_2', 'options') ): the_row();
				$title = get_sub_field('col2_titel');
		?>

			<ul class="col-sm-6 col-md-3">
				<li>
					<h3><?php echo $title; ?></h3>
				</li>

				<?php
				if( have_rows('col2_links', 'options') ):

					while ( have_rows('col2_links', 'options') ) : the_row();

						$link = get_sub_field('link');
						echo '<li><a href="' . $link['url'] . '" target="'. $link['target'] .'" >' . $link['title'] . ' </a></li>';

					endwhile;

				else :
				endif;
				?>
				<li class="gap"></li>
			</ul>


		<?php endwhile; endif; ?>
		<!-- END OF COLUMN 2 -->


		<!-- START OF COLUMN 3 -->
		<?php
			if( have_rows('footer_col_3', 'options') ):
				while( have_rows('footer_col_3', 'options') ): the_row();
				$title = get_sub_field('col3_titel');
		?>
			<ul class="col-sm-6 col-md-3">
				<li>
					<h3><?php echo $title ?></h3>
				</li>

				<?php
				if( have_rows('col3_links', 'options') ):

					while ( have_rows('col3_links', 'options') ) : the_row();

						$link = get_sub_field('link');
						echo '<li><a href="' . $link['url'] . '" target="'. $link['target'] .'" >' . $link['title'] . ' </a></li>';

					endwhile;

				else :
				endif;
				?>
				<li class="gap"></li>
			</ul>
			<?php endwhile; endif; ?>
			<!-- END OF COLUMN 3 -->

			<!-- START OF COLUMN 4 -->
			<?php
				if( have_rows('footer_col_4', 'options') ):
					while( have_rows('footer_col_4', 'options') ): the_row();
					$title = get_sub_field('col4_titel');
			?>
			<ul class="col-sm-6 col-md-3">
				<li>
					<h3><?php echo $title ?></h3>
				</li>

				<?php
				if( have_rows('col4_methods', 'options') ):

					while ( have_rows('col4_methods', 'options') ) : the_row();

						echo    '<li><img src="';
						echo    get_sub_field('afbeelding');
						echo    '" alt=""></li>';

					endwhile;

				else :
				endif;
				?>

				<li class="app-store">

				<?php
				if( have_rows('col4_app', 'options') ):

					while ( have_rows('col4_app', 'options') ) : the_row();

						$link = get_sub_field('link');
						$link_url = $link[url];
						$link_title = $link[title];
						$link_target = $link[target];
						$image = get_sub_field('afbeelding');
						echo    '<a href="';
						echo    $link_url;
						echo    '" target="';
						echo    $link_target;
						echo    '"><img src="';
						echo    $image;
						echo    '" alt=""></a>';

					endwhile;

				else :
				endif;
				?>

				</li>
			</ul>
			<?php endwhile; endif; ?>
			<!-- END OF COLUMN 4 -->

		</div>

		<?php
			if( have_rows('footer_social', 'options') ):
				while( have_rows('footer_social', 'options') ): the_row();
				$tekst = get_sub_field('social_tekst');
		?>
			<div class="row social">
				<ul>
					<?php
					if( have_rows('social_links', 'options') ):

						while ( have_rows('social_links', 'options') ) : the_row();
						$link = get_sub_field('link');
						$icon = get_sub_field('icoon');
					 ?>
					 	<li>
					 		<a href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>">
								<img src="<?php echo $icon ?>" alt="">
					 		</a>
					 	</li>

					 <?php endwhile; else: ?>
							<li><a href="https://www.facebook.com/VacuVita/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets-front/img/icons/facebook.png" alt=""></a></li>
							<li><a href="https://twitter.com/VacuVita" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets-front/img/icons/twitter.png" alt=""></a></li>
							<li><a href="https://www.instagram.com/vacuvita/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets-front/img/icons/instagram.png" alt=""></a></li>
					<?php endif; ?>
					<li class="share-page"><a href="#socail_share" data-lity>
					<?php echo $tekst; ?>

					</a></li>
				</ul>
			</div>
		<?php endwhile; endif; ?>

	</div>
</footer>
<!-- End Footer -->

<?php
	wp_reset_query();  // Restore global post data stomped by the_post().
?>


<?php
//LOAD HEADER SCRIPTS
if( have_rows('scripts', 'option') ):
    while( have_rows('scripts', 'option') ): the_row();
        $locatie = get_sub_field('locatie');
        if ($locatie == 'Footer') {
            echo    get_sub_field('script');
        }
    endwhile;
endif;
?>




<?php

	// include('site-localization.php');
?>

	<div class="language-selector">

<ul>


	<?php
		foreach($localized_sites as $localized_site => $value) :
			if($localized_site != 1) : ?>
			<li>
				<a href="<?php echo get_site_url($localized_site); ?>">

					<img src="<?php echo content_url() ?>/plugins/sitepress-multilingual-cms/res/flags/<?php echo $value['attr']['flag'] ?>.png" alt="<?php echo $value['attr']['flag'] ?>" title="<?php echo $value['attr']['title'] ?>">

					<span class="language">
						<?php echo $value['attr']['title'] ?>
					</span>
				</a>
			</li>

			<?php
			endif;
		endforeach;
 	?>

</ul>
</div>
<?php wp_footer(); ?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

<script>
	AOS.init({
		offset: ($(window).height() / 4),
		once: !0,
	});
</script>
</body>
</html>
