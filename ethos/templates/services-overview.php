<?php
/*
Template Name: default page builder
*/

get_header();

?>

<?php
if (have_posts()) :
    while (have_posts()) :
    the_post();

    if( have_rows('content_sections') ):
        while ( have_rows('content_sections') ) : the_row();


            if( get_row_layout() == 'home_header' ):

                get_template_part('templates-parts/intro', 'intro');

            elseif(get_row_layout() == 'about'):

                get_template_part('templates-parts/about', 'about');

            elseif(get_row_layout() == 'services'):

                get_template_part('templates-parts/services', 'services');

            elseif(get_row_layout() == 'cta'):

                get_template_part('templates-parts/cta', 'cta');

            elseif(get_row_layout() == 'payoff'):

                get_template_part('templates-parts/payoff', 'payoff');

            elseif(get_row_layout() == 'textblock'):

                get_template_part('templates-parts/textblock', 'textblock');
            
            elseif(get_row_layout() == 'visual_cue'):
                
                get_template_part('templates-parts/visualcue', 'visualcue');

            elseif(get_row_layout() == 'overview'):

                get_template_part('templates-parts/services_overview', 'services overview');

            elseif(get_row_layout() == 'end'):

                get_template_part('templates-parts/services_end', 'end of page');

            endif;


        endwhile;

    else :
    endif;

 endwhile;
endif;

?>

<?php
get_footer(); ?>
