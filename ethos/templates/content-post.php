<?php if ( is_home() || is_archive() ) : // ARCHIVE ?>
    <?php if (function_exists('z_taxonomy_image_url')) { ?>
        <?php if(z_taxonomy_image_url()) { ?>
            <div class="himage-wrap intro">
                <div class="absolute container intro__container"><span class="section-tag intro__tag"><?php echo get_the_archive_title(); ?></span></div>
                <img src="<?php echo z_taxonomy_image_url(); ?>" class="img-responsive" id="header-image">
            </div>
            <?php } else { ?>
                <div class="himage-wrap intro">
                    <div class="absolute container intro__container"><span class="section-tag intro__tag"><?php if(is_category()) { echo get_the_archive_title(); } else { echo 'Blog'; } ?></span></div>
                    <img src="https://d3oosua12zijzz.cloudfront.net/wp-content/uploads/2016/09/18183746/Vacuum-Storage-Food-Storing-Vacuvita.jpg" class="img-responsive" id="header-image">
                </div>
            <?php } ?>
        <?php } ?>

<section class="page-body archive-body container">
  <div class="row">
	 <div class="col-md-9 col-sm-12 col-xs-12 page-content blog">

  <?php if ( have_posts() ) : ?>
    <ol id="posts" class="list col-xs-12 hfeed blog__overview" start="1">

    <?php while ( have_posts() ) : the_post(); ?>
      <li class="row blog__row">
        <article itemscope itemtype="http://schema.org/BlogPosting" <?php post_class(); ?>>
	        <?php if ( has_post_thumbnail() ) { ?>
          <header class="entry-header col-sm-8 col-sm-offset-4 blog__header blog__header--overview">
	        <?php } else { ?>
	        <header class="entry-header">
	        <?php } ?>
            <h2 class="entry-title blog__title blog__title--overview">
                <a href="<?php echo get_permalink( $post->ID ); ?>" class="blog__link blog__link--overview">
                    <?php the_title(); ?>
                </a>
            </h2>
            <time class="published blog__datewrapper" datetime="<?php echo get_the_date(); ?>">
                <span class="entry-date blog__date">
                    <?php echo get_the_date('F d, Y'); ?>
                </span>
            </time>
          </header>
          <?php if ( has_post_thumbnail() ) {?>
          	<figure class="entry-thumb col-sm-4 blog__thumbnail">
          	     <?php  the_post_thumbnail('thumb', array('class' => 'img-reposponsive blog__thumbnail-image')); ?>
          	</figure>
          	<div class="entry-body col-sm-8 blog__body">
          <?php } else { ?>
	          <div class="entry-body blog__body">
          <?php } ?>

            <?php the_excerpt(); ?>
            <footer class="entry-meta blog__bottom">
            	<a rel="bookmark" href="<?php the_permalink(); ?>" class="txt-blue blog__link blog__link--read-more">
                    <?php echo 'Read more'; ?>

                    </a>
            </footer>
          </div>

        </article>
      </li>
    <?php endwhile; ?>
    </ol>
    <?php if(function_exists('tw_pagination')) : ?>
    <div class="col-xs-12 no-padding">
    	<?php tw_pagination(); ?>
    </div>
    <?php endif; ?>
	 </div><!-- /.page-content -->
	 <div class="col-md-3 col-sm-12 col-xs-12">
   	 <?php include(locate_template( 'templates/sidebar.php' ) ); ?>
	 </div>

  </div><!-- /.row -->
  <?php else : ?>
    <p>No Posts.</p>
  <?php endif; ?>
</section>

<div id ="container">
<?php //include(TEMPLATEPATH .'/static/php/page-footer.php'); ?>
</div>

<?php else : // SINGLE ?>

<?php $query = new WP_Query( array(
		'post_type' => 'post',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'date', // be sure posts are ordered by date
    'order' => 'DESC', // be sure order is ascending
    'fields' => 'ids' // get only post ids
));

global $post; // current post object
global $custom_options; //used for getting nieuws page if
$news_pageID = $custom_options['news_page'];

$i = array_search( $post->ID, $query->posts ) + 1; // add 1 because array are 0-based
if( $i > get_option('posts_per_page') ) {
	$page_num = ceil( $i / get_option('posts_per_page')) ;
}
 if ( $page_num ) {
	 $backto_link = get_post_permalink( $news_pageID, false, true ).'&paged='.$page_num;
 } else {
	 $backto_link = get_page_link($news_pageID);
 }

//echo "Post {$i} / {$query->post_count}";

?>

<section class="page-body archive-body container blogwrap">
<article itemscope itemtype="http://schema.org/Article" <?php post_class('single-details'); ?> >
	<a href="<?php echo get_category_link( 27 ); ?>" class="txt-orange backto">&laquo; back to archive</a>
  <div class="page-body postBody">
	 <div class="row">
	  <div class="col-sm-9 page-content blog">
		   <header class="blog__header">

				 <h1 class="page-title txt-blue postTitle blog__title blog__title--single">
                     <?php the_title(); ?>
                 </h1>

		    <time class="published blog__datewrapper" datetime="<?php echo get_the_date(); ?>">
                <span class="entry-date blog__date">
                    <?php echo get_the_date('F d, Y'); ?>
                </span>
            </time>



				<?php if ( get_field('page_header_image') ) {
						//$image_id = vacuvita_get_image_id( get_field('page_header_image') );
					echo '<figure class="entry-thumb postImage blog__image">
						<img src="'.get_field('page_header_image').'" data-lazy-type="image" data-lazy-src="'.get_field('page_header_image').'" class="lazy attachment-post-thumbnail size-post-thumbnail wp-post-image img-responsive" alt="'.get_the_title($image_id).'">
					</figure>';
				}?>
  		</header>
	    <?php while ( have_posts() ) : the_post(); the_content(); endwhile; ?>
	    <div class="postSocial row blog__social">
		    <div class="col-sm-6">
			    <span class="shareText">Share This Blog Post!</span>
		    </div>
		    <div class="col-sm-6">
				  <?php echo do_shortcode('[ssba]'); ?>
		    </div>
	    </div><!-- /.postSocial -->
	  </div><!-- /.page-content -->
	  <div class="col-md-3 col-sm-12">
   	 <?php include(locate_template( 'templates/sidebar.php' ) ); ?>
	  </div>
  	</div>
  </div><!-- /.row -->
  <footer class="blog__bottom">
  </footer>
</article>
</section><!-- /.container -->
<?php //include(TEMPLATEPATH .'/static/php/page-footer.php'); ?>
<?php endif;
