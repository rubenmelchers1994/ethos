<?php // $json_results = json_decode(file_get_contents(get_stylesheet_directory_uri().'/static/php/results.json')); ?>
<?php if(get_field('page_header_image')) { ?>
<header id="main-header">
   <div class="row himage-wrap">
     <div class="col-sm-12">

        <img src="<?php echo get_field('page_header_image'); ?>" class="img-responsive" id="header-image" />

       <?php if(get_field('page_subtitle')) { ?>
                <div class="absolute container headerText"><?php echo get_field('page_subtitle'); ?></div>
          <?php } ?>
     </div>
   </div>
   <!-- /row -->

 </header>
 <!-- /header -->
<?php } ?>

 <div class="container">
   <section class="main-section pageContent">
       <?php if ( have_posts() ) : ?>
           <h1 class="section-title"><?php echo get_the_title(); ?></h1>
               <?php the_content(); ?>
           <?php else : ?>
               <h1 class="section-title">404 - Wrong URL</h1>
               <?php get_search_form(); ?>
           <?php endif; ?>
 </section>
</div><!--!/#container -->
<?php
    // include(TEMPLATEPATH .'/static/php/page-footer.php');
?>
