<?php
/*
Template Name: Default
*/

get_header();

?>

<?php
if (have_posts()) :
    while (have_posts()) :
    the_post();

    if( have_rows('content_items') ):
        while ( have_rows('content_items') ) : the_row();

            if( get_row_layout() == 'content_header' ):

                get_template_part('templates-parts/default-header', 'header');

            elseif(get_row_layout() == 'content_block-with-sidebar'):

                get_template_part('templates-parts/content_block-with-sidebar', 'block-with-sidebar');

            elseif(get_row_layout() == 'content_image-text'):

                get_template_part('templates-parts/image_with_text', 'image_with_text');

            elseif(get_row_layout() == 'content_large-cta'):

                get_template_part('templates-parts/large-cta', 'large-cta');

            elseif(get_row_layout() == 'content_option-blocks'):

                get_template_part('templates-parts/option-blocks', 'option-blocks');

            elseif(get_row_layout() == 'color-picker-with-tab'):

                get_template_part('templates-parts/color-picker-with-tab', 'color-picker-with-tab');

            elseif(get_row_layout() == 'content_reviews-signup'):

                get_template_part('templates-parts/content_reviews-signup', 'reviews-and-signup');

            elseif(get_row_layout() == 'content_image-tab'):

                get_template_part('templates-parts/content-image-tab', 'image_with_tab');

            elseif( get_row_layout() == 'home_product-promotion' ):

                get_template_part( 'templates-parts/product-promotion', 'product-promotion' );

            elseif( get_row_layout() == 'content_banner' ):

                get_template_part('templates-parts/banner', 'banner');

            elseif( get_row_layout() == 'content_prices' ):

                get_template_part('templates-parts/prices', 'prices');

            elseif( get_row_layout() == 'content_products' ):

                get_template_part('templates-parts/products', 'products');

            elseif( get_row_layout() == 'content_reviews' ):

                get_template_part('templates-parts/reviews', 'reviews');

            elseif( get_row_layout() == 'content_blogs' ):

                get_template_part('templates-parts/blogs', 'blogs');

            elseif( get_row_layout() == 'home_huidverbetering' ):

                get_template_part('templates-parts/celyo_home_huidverbetering', 'celyo_home_huidverbetering');

            elseif( get_row_layout() == 'content_tekst' ):

                get_template_part('templates-parts/tekst', 'tekst');

            elseif( get_row_layout() == 'content_cta' ):

                get_template_part('templates-parts/cta', 'cta');

            elseif( get_row_layout() == 'content_gallerij' ):

                get_template_part('templates-parts/gallery', 'gallery');

            elseif( get_row_layout() == 'content_huidverbetering_info' ):

                get_template_part('templates-parts/cleyo_huidverbetering', 'cleyo_huidverbetering');

            elseif( get_row_layout() == 'content_team' ):

                get_template_part('templates-parts/team', 'team');

            elseif( get_row_layout() == 'content_contact' ):

                get_template_part('templates-parts/contact', 'contact');

            elseif( get_row_layout() == 'google_maps' ):

                get_template_part('templates-parts/googlemaps', 'googlemaps');

            endif;


        endwhile;

    else :
    endif;

 endwhile;
endif;

?>

<?php
get_footer(); ?>
