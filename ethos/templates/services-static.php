<?php
/*
Template Name: Services Static
*/

$titel = get_field('services_overview_title');
$description = get_field('services_overview_description');

$query = new WP_Query( 'cat=Service' );

global $service;
$arguments = array(
    'category' => 'Service',
    'order' => 'ASC'
);
$services = get_posts($arguments);

get_header('static');

?>

<section class="intro intro--services-page">
    <div class="grid-12 container">

        <?php if($titel) : ?>
            <div class="col-12 intro__title">
                <h1>
                    <?php echo $titel; ?>
                </h1>
            </div>
        <?php endif; ?>

        <?php if($description) : ?>
            <div class="col-12 intro__description">
                <h3>
                    <?php echo $description; ?>
                </h3>
            </div>
        <?php endif; ?>

        <div class="col-12 visual-cue">
            <a href="#overview" class="visual-cue__anchor"></a>
        </div>
    </div>
</section>

<section id="overview" class="overview">

        <?php if($services) : ?>
            <?php $counter = 0; ?>
            <div class="grid-12 container">
            <?php foreach($services as $service) : setup_postdata( $service ); ?>

                <?php
                $class = '';
                if($counter != 0) {
                    $class = 'overview__service-wrapper--right';
                }
                //set variables
                $service_title      = get_field('title', $service->ID);
                $service_img        = get_field('image', $service->ID);
                $service_desc       = get_field('description', $service->ID);
                $service_price      = get_field('cost', $service->ID);
                $service_time       = get_field('time', $service->ID);
                $service_contact    = get_field('contact', $service->ID);
                ?>
                <div class="col-6 col_sm-12 overview__service-wrapper <?php echo $class; ?>" style="background-image: url('<?php echo $service_img ?>');">
                    <h3 class="overview__service-title">
                        <?php echo $service_title ?>
                    </h3>
                </div>
            <?php
                if($counter > 0) {
                    $counter = 0;
                } else {
                    $counter += 1;
                }
            ?>
            <?php endforeach; wp_reset_postdata(); ?>
        </div>
    <?php endif; ?>

</section>


<?php
get_footer('static'); ?>
