<?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>
	<ul class="sidebar-blog">
		<?php dynamic_sidebar( 'sidebar-blog' ); ?>
	</ul>
<?php endif; ?>
<ul class="sidebar-blog">
		<h3 class="tc-title">Categories</h3>
		<?php
		foreach((get_categories()) as $category) {
    	//if ($category->cat_name != 'Cooking') {
				echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a></li> ';
			//}
		}
	?>
</ul>
<?php if(is_singular()) : ?>
<?php
	$tags = wp_get_post_terms( get_queried_object_id(), 'post_tag', array('fields' => 'ids') );
	$args = array(
	    'post__not_in'        => array( get_queried_object_id() ),
	    'posts_per_page'      => 5,
	    'ignore_sticky_posts' => 1,
	    'orderby'             => 'rand',
	    'tax_query' => array(
	        array(
	            'taxonomy' => 'post_tag',
	            'terms'    => $tags
	        )
	    )
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ) { ?>
	    <div id="related">
		    <h3 class="tc-title">Related Posts</h3>
	        <?php while( $my_query->have_posts() ) {
	            $my_query->the_post(); ?>
	            <div class="ncc">

	                <a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>" rel="nofollow"><?php the_title(); ?></a>

	            </div><!--ncc-->
	        <?php }
	        wp_reset_postdata(); ?>
	    </div><!-- /#related -->
<?php	} ?>
<?php endif; ?>

<?php if ( function_exists( 'wp_tag_cloud' ) ) : ?>
<div class="cloud sidebar-blog">
	<h3 class="tc-title">Tag Cloud</h3>
	<ul>
		<li><?php wp_tag_cloud( array('smallest' =>10, 'largest'=>16, 'taxonomy' => 'post_tag', 'number'=> 20, 'exclude' => array(41,38)  )); ?></li>
	</ul>
</div>
<?php endif; ?>
