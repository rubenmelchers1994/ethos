<?php
/*
Template Name: Home
*/

get_header();

?>

<?php
if (have_posts()) :
    while (have_posts()) :
    the_post();

    if( have_rows('content_sections') ):
        while ( have_rows('content_sections') ) : the_row();

            if( get_row_layout() == 'header' ):

                get_template_part('templates-parts/header', 'header');

            elseif(get_row_layout() == 'about'):

                get_template_part('templates-parts/about', 'about');

            elseif(get_row_layout() == 'process'):

                get_template_part('templates-parts/process', 'process');

            elseif(get_row_layout() == 'folio'):

                get_template_part('templates-parts/folio', 'folio');

            elseif(get_row_layout() == 'partners'):

                get_template_part('templates-parts/partners', 'partners');

            elseif(get_row_layout() == 'contact'):

                get_template_part('templates-parts/contact', 'contact');

            endif;


        endwhile;

    else :
    endif;

 endwhile;
endif;

?>

<?php
get_footer(); ?>
