<?php
/*
* Template Name: Service Detail
* Template Post Type: post, page, product
*/

get_header();

?>

<?php
if (have_posts()) :
    while (have_posts()) :
    the_post();

    if( have_rows('content_sections') ):
        while ( have_rows('content_sections') ) : the_row();

            if( get_row_layout() == 'intro' ):

                get_template_part('templates-parts/intro', 'intro');

            elseif(get_row_layout() == 'body'):

                get_template_part('templates-parts/service-body', 'body');

            elseif(get_row_layout() == 'form'):

                get_template_part('templates-parts/service-form', 'form');

            elseif(get_row_layout() == 'cta'):

                get_template_part('templates-parts/cta', 'cta');
            
            elseif(get_row_layout() == 'visual_cue'):
                
                get_template_part('templates-parts/visualcue', 'visualcue');

            endif;


        endwhile;

    else :
    endif;

 endwhile;
endif;

?>

<?php
get_footer(); ?>
