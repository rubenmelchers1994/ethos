

<section class="header">
    <div class="grid-12 container">
        <div class="header__blocks-wrapper grid-12">

        </div>
        

        <div class="col-9 col_sm-12 header__inner">
            <h1 class="header__title">
                We make your <span class="header__highlighted">responsible</span> brand stand out
            </h1>
        </div>
    </div>
</section>