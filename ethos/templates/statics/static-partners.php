<section class="partners">
    <div class="grid-12 container partners__wrapper">
        
        <div class="col-12 partners__title-wrapper">
            <h2 class="partners__title">
                Current clients & partners
            </h2>
        </div>

        <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-100">
            <div class="partners__logo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/partners/bluecity.png');"></div>

        </div>

        <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-200">
            <div class="partners__logo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/partners/parkbee.png');"></div>

        </div>

        <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-300">
            <div class="partners__logo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/partners/soephoofd.png');"></div>

        </div>

        <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-400">
            <div class="partners__logo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/partners/manatee.png');"></div>

        </div>

        <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-500">
            <div class="partners__logo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/partners/food-footnotes.png');"></div>

        </div>



    </div>
</section>