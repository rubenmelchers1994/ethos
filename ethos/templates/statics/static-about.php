

<section class="about" id="ethos">
    <div class="grid-12 container">

        <div class="col-12">
            <h2 class="about__title about__title--small">
                Ethos
            </h2>
        </div>
        
        <div class="col-4 col_sm-12 about__image-wrapper">
            <div class="about__image-blocks fadeInDown delay-200">

            </div>

            <div class="about__image-head fadeInUp delay-300"></div>

        </div>

        <div class="col-6 col_sm-12 about__content-wrapper">
            <h2 class="about__title">
                Ethos
            </h2>
            <p class="about__text">
                Stukje over Ethos, waarom dit beginnen? Wat is er mis met ontwerpen nu. Mijn focus op de branding van sustainable / responsible business & products. + het ontwikkelen van duurzame verpakkings- -mogelijkheden om zo de negatieve impact van verpakking op het milieu te verminderen.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
            </p>
        </div>
    </div>
</section>