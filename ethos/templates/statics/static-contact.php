<section class="contact" id="contact">
    <div class="grid-12 container">
        
        <div class="col-12 contact__title-wrapper">
            <h2 class="contact__title">
                Contact
            </h2>
        </div>

        <div class="col-12 contact__description-wrapper">
            <p class="contact__description">
                Goed gevoel? Laat een bericht achter en we maken een afspraak voor een kennigsmakingsgesprek.
            </p>
        </div>

        <form action="#" class="col-12">

            <div class="grid-12">

                <div class="col-6 contact__input-name">
                    <label for="naam">
                        <input type="text" placeholder="naam" id="naam" name="naam">
                    </label>
                </div>

                <div class="col-12 contact__input-message">
                    <label for="message">
                        <textarea placeholder="bericht" id="bericht" name="bericht" > </textarea>
                    </label>
                </div>

                <div class="col-4 contact__submit">
                    <input type="submit" value="Verstuur">
                </div>

            </div>

        </form>



    </div>
</section>