<section class="process">
    <div class="grid-12 container">
        

        <div class="col-7 col_sm-12 process__content-wrapper">
            <h2 class="process__title">
                How we work
            </h2>
            <div class="process__text-wrapper">
                <div class="process__text fadeInUp">
                    <strong class="process__strong">Logos</strong> - We beginnen met het defineren van de onderliggende waarheid van het bedrijf. Van welk verhaal is deze startup of ondernemer de protagonist, wat is er aan de hand in de wereld dat dit nodig is. <br/>

                </div>
                <div class="process__text fadeInUp delay-200">
                    <strong class="process__strong">Pathos</strong> -  Vervolgens gaan we hier een subjectieve emotionele laag overheen leggen om zo de doelgroep te bereiken die we willen bereiken. Dmv specifieke visuele identiteit en emotionele storytelling. <br/>

                </div>
                <div class="process__text fadeInUp delay-400">
                    <strong class="process__strong">Ethos</strong> -  Dit bij elkaar maakt het bedrijf een verlengde van het karakter van de ondernemer. En zorgt dat de motivatie een eerlijk en oprecht is, niet per se voor economische doeleinden.           

                </div>

            </div>
        </div>

        <div class="col-4 col_sm-12 process__content-wrapper process__content-wrapper--visual">
            <div class="process__visual">
                <h2 class="process__visual-title process__visual-title--first fadeInUp delay-150">
                    <span class="process__visual-title-inner">
                        logos
                    </span>
                </h2>
                <div class="process__visual-arrow process__visual-arrow--first fadeInUp delay-200">

                </div>
                <h2 class="process__visual-title process__visual-title--second fadeInUp delay-350">
                    <span class="process__visual-title-inner">
                        pathos
                    </span>
                </h2>
                <div class="process__visual-arrow process__visual-arrow--second fadeInUp delay-400">

                </div>
                <h2 class="process__visual-title process__visual-title--third fadeInUp delay-550">
                    <span class="process__visual-title-inner">
                        ethos
                    </span>
                </h2>
            </div>
        </div>
    </div>
</section>