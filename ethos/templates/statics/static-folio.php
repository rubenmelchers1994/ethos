<section class="folio" id="folio">
    <div class="folio__title-wrapper">
        <div class="grid-12 container">
            <h2 class="folio__title fadeInLeft">
                Folio
            </h2>
        </div>
    </div>
    <div class="grid-12 folio__wrapper">
        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link" data-folio="1">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/mockups.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>

        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link" data-folio="2">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/food-foodnotes-mockup.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>

        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link" data-folio="3">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/future-flight.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>

        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/weggooigroentesoep.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>

        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/mockups.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>

        <div class="col-4 col_md-6 col_sm-12 folio__block">
            <a href="#" class="folio__link">
                <div style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/mockups.png)" class="folio__image" > </div>
                <h2 class="folio__project-title">
                    lorem
                </h2>

            </a>

        </div>



    </div>
    <?php 
        include('folio/template-1.php');
        include('folio/template-2.php');
    ?>
</section>
