<div class="modal" data-folio-modal="1">
    <div class="modal__intro">
        <p>
            <strong class="modal__strong">
                Branding Plastic Diet Month Kickoff Event
            </strong>
            <br><br>
                My client asked me to design posters and come up with a concept for an event hosted by BlueCity. The event is a kickoff for the "Plastic Diet Month", this is a month where participants are challenged to live one month without consuming any plastics at all. They asked me to design posters which would attract the interest of local entrepreneurs and citizens of Rotterdam in general.
        </p>

    </div>

    <div class="modal__images">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-1.jpg" class="modal__img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-2.jpg" class="modal__img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-3-small.jpg" class="modal__img modal__img--small">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-4-small.jpg" class="modal__img modal__img--small">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-5-full.jpg" class="modal__img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/folio-images/modal-image-6-full.jpg" class="modal__img">
    </div>
</div>
