<?php
global $page_slug, $post_type;

$page_slug = get_post_field( 'post_name', get_post() );
$post_type = get_post_field( 'post_type', get_post() );

include('site-localization.php');

function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

$logoLang = '';
if($siteLang == 'nl' || $siteLang == 'pl') {
    $logoLang = $siteLang;
} else {
    $logoLang = '';
}

?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>

    <?php
    //LOAD HEADER SCRIPTS
    if( have_rows('scripts', 'option') ):
        while( have_rows('scripts', 'option') ): the_row();
        $locatie = get_sub_field('locatie');
        if ($locatie == 'Body') {
            echo    get_sub_field('script');
        }
    endwhile;
endif;
?>

<?php
//LOAD HEADER SCRIPTS
if( have_rows('scripts', 'option') ):
    while( have_rows('scripts', 'option') ): the_row();
    $locatie = get_sub_field('locatie');
    if ($locatie == 'Header') {
        echo    get_sub_field('script');
    }
endwhile;
endif;
?>

<?php wp_head(); ?>

<meta property="article:author" content="Ethos" />
<link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo( 'name' ); ?> Feed" href="<?php echo esc_url( get_feed_link() ); ?>">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/logo.png" type="image/x-icon"/>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/logo.png" type="image/png"/>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

<meta property="article:author" content="KBNA" />
<link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo( 'name' ); ?> Feed" href="<?php echo esc_url( get_feed_link() ); ?>">


</head>
<body>
    <nav class="navigation">
        <ul class="grid-12 container navigation__list">
                <li class="col-4 navigation__list-item navigation__list-item--logo">
                    <a href="<?php echo home_url( '/' ); ?>" class="navigation__logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="ethos logo" class="navigation__logo-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-visual.png" alt="" class="navigation__logo-image navigation__logo-image--visual">
                    </a>
                </li>
                <li class="col-8">
                    <ul class="grid-12 navigation__sublist">
                        <?php $args = array(
                            'menu' => 'onepage-menu',
                            'container' => '',
                            'items_wrap' => '%3$s',
                            'menu_class' => 'navigation',
                            'link_before' => '<span class="navigation__link-inner">',
                            'link_after' => '</span>'
                            );
                            wp_nav_menu($args);
                        ?>
                        <!-- <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#ethos">
                                <span class="navigation__link-inner">
                                    Ethos
                                </span>
                            </a>
                        </li>
                        <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#folio">
                                <span class="navigation__link-inner">
                                    Folio
                                </span>
                            </a>
                        </li>
                        <li class="col-4 col_sm-12 navigation__list-item">
                            <a class="navigation__link" href="<?php echo home_url(); ?>#contact">
                                <span class="navigation__link-inner">
                                    Contact
                                </span>
                            </a>
                        </li> -->

                    </ul>
                </li>
        </ul>
        <div class="navigation__hamburger">
            <div class="bar"></div>
        </div>
    </nav>

    <!-- <nav class="navigation">
        <ul class="grid-12 container navigation__list"> -->

            <!-- <li class="col-2 navigation__list-item navigation__list-item--logo">
                <a href="<?php echo home_url( '/' . $logoLang ); ?>" class="navigation__logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo_visual.png" alt=""></a>
            </li> -->

            <?php

            // $args = array(
            //     'menu' => 'onepage-menu',
            //     'container' => '',
            //     'items_wrap' => '%3$s',
            //     'menu_class' => 'navigation',
            //     'link_before' => '<span class="navigation__text">',
            //     'link_after' => '</span>'
            // );
            // wp_nav_menu($args);
            // ?>
        <!-- </ul>
    </nav> -->

    <main role="main" data-type="<?php echo $post_type; ?>" class="<?php echo $page_slug; ?> <?php echo 'slug__'.$page_slug; ?> <?php echo 'type__'.$post_type; ?>">
