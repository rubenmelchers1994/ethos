</main>
<!-- Footer -->
<footer class="footer">

    <div class="grid-12 container">
        <div class="col-4 footer__copyright">
            © Copyright, 2018
        </div>
        <div class="col-8 footer__sitemap">
            <ul class="footer__list grid-12">
                <li class="footer__list-item col-4">
                    <a class="footer__link" href="#ethos">
                        <span>
                            Ethos
                        </span>    
                    </a>
                </li>
                <li class="footer__list-item col-4">
                    <a class="footer__link" href="#folio">
                        <span>
                            Folio
                        </span>    
                    </a>
                </li>
                <li class="footer__list-item col-4">
                    <a class="footer__link" href="#contact">
                        <span>
                            Contact
                        </span>    
                    </a>
                </li>
            </ul>
        </div>
    </div>

</footer>
<!-- End Footer -->

</div>
<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.min.js">

</script>

</body>
</html>
