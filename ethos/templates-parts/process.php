<?php
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $top = get_sub_field('top_text');
    $middle = get_sub_field('middle_text');
    $bottom = get_sub_field('bottom_text');
?>

<section class="process">
    <div class="grid-12 container">
        

        <div class="col-7 col_sm-12 process__content-wrapper">
            <h2 class="process__title">
                <?php echo $title; ?>
            </h2>
            <div class="process__text-wrapper">

                <?php if(have_rows('content')): ?>
                    <?php while(have_rows('content')): the_row(); ?>
                        <?php if(get_sub_field('subtitle')) : ?>
                        <?php 
                            $subtitle = get_sub_field('subtitle');
                            $subcontent = get_sub_field('content');
                        ?>
                        <div class="process__text fadeInUp">
                            <strong class="process__strong"><?php echo $subtitle; ?></strong> 
                            <?php echo $subcontent; ?>
                        </div>

                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-4 col_sm-12 process__content-wrapper process__content-wrapper--visual">
            <div class="process__visual">
                <h2 class="process__visual-title process__visual-title--first fadeInUp delay-150">
                    <span class="process__visual-title-inner">
                        <?php echo $top; ?>
                    </span>
                </h2>
                <div class="process__visual-arrow process__visual-arrow--first fadeInUp delay-200">

                </div>
                <h2 class="process__visual-title process__visual-title--second fadeInUp delay-350">
                    <span class="process__visual-title-inner">
                        <?php echo $middle; ?>
                    </span>
                </h2>
                <div class="process__visual-arrow process__visual-arrow--second fadeInUp delay-400">

                </div>
                <h2 class="process__visual-title process__visual-title--third fadeInUp delay-550">
                    <span class="process__visual-title-inner">
                        <?php echo $bottom; ?>
                    </span>
                </h2>
            </div>
        </div>
    </div>
</section>