<?php

    $title = get_sub_field('title') != "" ? get_sub_field('title') : " ";
    $partners = get_sub_field('folios');
    $id = get_sub_field('id');
    $index = 0;
    $dataIndex = 0;

?>
<section class="folio" <?php if($id){?>id="<?php echo $id;?>"<?php }?>>
    <div class="folio__title-wrapper">
        <div class="grid-12 container">
            <h2 class="folio__title fadeInLeft">
                <?php 
                    echo $title
                ?>
            </h2>
        </div>
    </div>

    <div class="grid-12 folio__wrapper">

        <?php if(have_rows('folios')): ?>
            <?php while(have_rows('folios')): the_row(); ?>

                <?php 
                    $index =  $index + 1;
                    //title, description, thumbnail, images (image, layout, shadow)
                    $folioTitle = get_sub_field('title');
                    $folioDescription = get_sub_field('description');
                    $folioThumbnail = get_sub_field('thumbnail');
                    $folioImages = get_sub_field('images');
                ?>
                <div class="col-4 col_md-6 col_sm-12 folio__block">
                    <a href="#" class="folio__link" data-folio="<?php echo $index; ?>">
                        <div style="background-image: url(<?php echo $folioThumbnail; ?>)" class="folio__image" > </div>
                        <h2 class="folio__project-title">
                            <?php echo $folioTitle; ?>
                        </h2>
                    </a>
                </div>
                

            <?php endwhile; ?>
        <?php endif; ?>


    </div>
    <?php if(have_rows('folios')): ?>
        <?php while(have_rows('folios')): the_row(); ?>

            <?php 
                $dataIndex = $dataIndex + 1;
                //title, description, thumbnail, images (image, layout, shadow)
                $folioTitle = get_sub_field('title');
                $folioDescription = get_sub_field('description');
                $folioThumbnail = get_sub_field('thumbnail');
                $folioImages = get_sub_field('images');
            ?>

            <div class="modal" data-folio-modal="<?php echo $dataIndex; ?>">
                <div class="modal__close js-modal-close">
                    
                </div>
                <div class="modal__inner">
                    <div class="modal__intro">
                        <p>
                            <strong class="modal__strong">
                                <?php echo $folioTitle; ?>
                            </strong>
                            <br><br>
                            <?php echo $folioDescription; ?>
                        </p>

                    </div>

                    <?php if(have_rows('images')) : ?>
                        <div class="modal__images">
                            <?php while(have_rows('images')) : the_row(); ?>
                                    
                                        <?php 
                                            $imageUrl = get_sub_field('image');
                                            $layout = get_sub_field('layout');
                                            $shadow = get_sub_field('shadow') ? 'modal__img--shadow' : '';
                                        ?>

                                        <img src="<?php echo $imageUrl ?>" class="modal__img <?php echo $layout; echo " "; echo $shadow; ?>" />


                                    
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
                

        <?php endwhile; ?>
    <?php endif; ?>
</section>
