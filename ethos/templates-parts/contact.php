<?php
    $title = get_sub_field('title') != "" ? get_sub_field('title') : " ";
    $description = get_sub_field('description');
    $form_code = get_sub_field('form_code');
    $id = get_sub_field('id');
?>

<section class="contact" <?php if($id){?>id="<?php echo $id;?>"<?php }?>>
    <div class="grid-12 container">
        
        <div class="col-12 contact__title-wrapper">
            <h2 class="contact__title">
                <?php echo $title; ?>
            </h2>
        </div>

        <div class="col-12 contact__description-wrapper">
            <p class="contact__description">
                <?php echo $description; ?>
            </p>
        </div>

        <?php echo do_shortcode($form_code); ?>

        <form action="#" class="col-12" style="display:none;">

            <div class="grid-12">

                <div class="col-6 contact__input-name">
                    <label for="naam">
                        <input type="text" placeholder="naam" id="naam" name="naam">
                    </label>
                </div>

                <div class="col-12 contact__input-message">
                    <label for="message">
                        <textarea placeholder="bericht" id="bericht" name="bericht" > </textarea>
                    </label>
                </div>

                <div class="col-4 contact__submit">
                    <input type="submit" value="Verstuur">
                </div>

            </div>

        </form>



    </div>
</section>