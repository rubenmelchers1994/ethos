<?php

$title = get_sub_field('titel');
$image = get_sub_field('afbeelding');

?>

<header class="intro" style="background-image: url(<?php if($image) { echo $image; }; ?>);">
    <div class="row intro__wrap">
        <div class="col-sm-12">
            <div class="container intro__container">
                <?php if($title) : ?>
                    <div class="intro__tag">
                        <?php echo $title ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
