<?php

    $title = get_sub_field('title');
    $text = get_sub_field('text');
    $subtitles = get_sub_field('subtitles');
    $image = get_sub_field('image');
    $scrollTo = get_sub_field('id');
    $button = get_sub_field('button');
    $buttontext = get_sub_field('buttontext');
    $position = get_sub_field('cue_position');
    $id = get_sub_field('section_id');
?>



<section class="intro <?php if($image) { ?> intro--image <?php } ?>" <?php if($id){?>id="<?php echo $id;?>"<?php }?> <?php if($image){?>style="background-image:url('<?php echo $image;?>')";<?php }?>>
    <div class="grid-12 container intro__container">

        <?php if($title) : ?>
            <div class="col-12">
                <h1 class="intro__title">
                    <?php echo $title; ?>
                </h1>

                <?php if(count($subtitles)) : ?>
                    <ul class="intro__subtitles-list">
                        <?php if(have_rows('subtitles')): ?>
                            <?php while(have_rows('subtitles')): the_row(); ?>
                                <?php if(get_sub_field('subtitle_text')) : ?>

                                    <li class="intro__subtitle-item">
                                        <h3 class="intro__subtitle"><?php echo get_sub_field('subtitle_text') ?></h3>
                                    </li>

                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if($button) : ?>
            <div class="col-12 button">
                <div class="button__wrapper">
                    <a href="<?php echo $button['url']; ?>" class="button__link">
                        <span class="button__text">
                            <?php echo $button['title']; ?>
                        </span> 
                    </a>
                </div>
            </div>
        <?php endif; ?>

        <?php if($text): ?>
            <div class="col-12">
                <p class="intro__text">
                    <?php echo $text; ?>
                </p>
            </div>
        <?php endif; ?>

    </div>

</section>
