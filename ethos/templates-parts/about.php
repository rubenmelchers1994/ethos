<?php

    $image = get_sub_field('image');
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $id = get_sub_field('id');


?>

<section class="about" <?php if($id){?>id="<?php echo $id;?>"<?php }?>>
    <div class="grid-12 container">

        <div class="col-12">
            <h2 class="about__title about__title--small">
                <?php echo $title; ?>
            </h2>
        </div>
        
        <div class="col-4 col_sm-12 about__image-wrapper">
            <div class="about__image-blocks fadeInDown delay-200">

            </div>

            <div class="about__image-head fadeInUp delay-300" style="background-image:url('<?php echo $image; ?>');"></div>

        </div>

        <div class="col-6 col_sm-12 about__content-wrapper">
            <h2 class="about__title">
                <?php echo $title; ?>
            </h2>
            <p class="about__text">
                <?php echo $content; ?>
            </p>
        </div>
    </div>
</section>