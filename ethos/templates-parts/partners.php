<?php

    $title = get_sub_field('title');
    $partners = get_sub_field('partners');

?>

<section class="partners">
    <div class="grid-12 container partners__wrapper">
        
        <div class="col-12 partners__title-wrapper">
            <h2 class="partners__title">
                <?php echo $title; ?>
            </h2>
        </div>

        <?php if(have_rows('partners')): ?>
            <?php while(have_rows('partners')): the_row(); ?>
                <?php if(get_sub_field('image')) : ?>
                    <?php $image = get_sub_field('image'); ?>
                    <?php if(get_sub_field('animation_delay')) : ?>
                        <?php $animDelay = get_sub_field('animation_delay'); ?>
                    <?php else : ?>
                        <?php $animDelay = 0 ?>
                    <?php endif ?>
                    <div class="col-2 col_sm-6 col_xs-12 partners__block-wrapper fadeIn delay-<?php echo $animDelay; ?>">
                        <div class="partners__logo" style="background-image:url('<?php echo $image; ?>);"></div>
                    </div>

                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>

    </div>
</section>