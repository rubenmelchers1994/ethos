<?php

    $title = get_sub_field('title');
    $highlighted = get_sub_field('highlighted_word');
?>
<section class="header">
    <div class="grid-12 container">
        <div class="header__blocks-wrapper grid-12">

        </div>
        

        <div class="col-9 col_sm-12 header__inner">
            <h1 class="header__title" data-highlighted="<?php echo $highlighted; ?>">
                <?php echo $title; ?>
            </h1>
        </div>
    </div>
</section>
