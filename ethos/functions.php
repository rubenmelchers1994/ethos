<?php

//****************//
// Include functions:
// Scripts, JS & CSS files
// Custom posts, create custom posts
// Config, all config setting regarding the admin backend
// Contactform 7, all settings regarding Contact Form 7

// Function 1: Dublicate pages
// Function 2: Login Pitcher Branding
// Function 3: Disable auto updates
// Function 4: Register menu's
// Function 5: Custom Excerpt function for Advanced Custom Fields
//****************//


// Include theme functions
// include_once 'theme-functions/config-builder.php';
// include_once 'theme-functions/scripts.php';
// include_once 'theme-functions/custom-posts.php';
// include_once 'theme-functions/config.php';
// include_once 'theme-functions/contact-form7.php';
// include_once 'theme-functions/menus.php';
// include_once 'theme-functions/woocommerce.php';
// include_once 'theme-functions/taxonomies.php';


// Function 1: Dublicate pages
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;

	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	$post = get_post( $post_id );
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	if (isset( $post ) && $post != null) {

		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		$new_post_id = wp_insert_post( $args );

		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}

		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}

add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );

add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);

// End


// Function 2: Login with Ethos Branding
function my_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/theme-files/login/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');

    function my_login_logo_url() {
return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
// add_action( 'init', 'create_posttype' );

// End


// Function 3: Disable auto updates
add_filter( 'auto_update_plugin', '__return_false' );


function remove_dashboard_meta() {
        remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'remove_dashboard_meta' );

// End


/*-----------------------------------------
	EXCERPT
-----------------------------------------*/
function custom_excerpt_length( $length ) {
	return 85;
}
function custom_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
add_filter( 'excerpt_more', 'custom_excerpt_more' );



/*-----------------------------------------
			POST TAGS
-----------------------------------------*/
// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'post');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');


add_theme_support( 'menus' );



?>
